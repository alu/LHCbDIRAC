"""
   LHCbDIRAC.Core.Utilities package

   This contains the LHCb specific utilities.
"""

__RCSID__ = "$Id$"
